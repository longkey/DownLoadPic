import sqlite3

class dbutil():

	def __init__(self):
		self.cur=sqlite3.connect("forpic.db")
		self.cur.execute("create table if not exists picture(id integer primary key autoincrement,link varchar,visited integer)")
		self.cur.execute("create table if not exists link(id integer primary key autoincrement,link varchar,visited integer)")

	def query(self,sql):
		"""
		select data to a list
		"""
		result=list(self.cur.execute(sql))
		self.cur.close()
		return result

	def update(self,sql):
		"""
		commit a sql
		"""
		result=self.cur.execute(sql)
		self.cur.commit()
		self.cur.close()
