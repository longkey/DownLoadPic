import time
class logutil():
	"""write some log file"""
	def __init__(self, logname):
		self.logname = logname

	def log(self,content):
		"""write log to file"""
		with open(self.logname,'a')as f:
			f.write('\n')
			f.write(time.strftime('%Y/%m/%d %H:%M:%S',time.localtime(time.time()))+" "+content)
		
